const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { studentsService } = require("./services/students.service");
const { validateUserData } = require("./utils/validateStudentData");

const app = express();
const port = 8000;

// CORS middleware
app.use(cors());

// Body parser middleware
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.post("/student/add", async (req, res) => {
  try {
    const error = validateUserData(req.body);
    if (error) {
      res.send({
        success: false,
        message: error,
      });
      return;
    }

    const data = await studentsService.addStudent(req.body);

    res.send({
      success: true,
      data,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error adding student",
    });
  }
});

app.post("/student/edit/:id", async (req, res) => {
  try {
    const error = validateUserData(req.body);
    if (error) {
      res.send({
        success: false,
        message: error,
      });
      return;
    }

    const data = await studentsService.editUserById(req.params.id, req.body);

    res.send({
      success: true,
      data,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error editing student",
    });
  }
});

app.delete("/student/delete/:id", async (req, res) => {
  try {
    const studentId = await studentsService.deleteStudent(req.params.id);

    res.send({
      success: true,
      data: studentId,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error deleting student with this id",
    });
  }
});

app.get("/student/all", async (req, res) => {
  try {
    const allStudents = await studentsService.getAllStudents();

    res.send({
      success: true,
      data: allStudents,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Can't get all students",
    });
  }
});

app.get("/student/:id", async (req, res) => {
  try {
    const studentData = await studentsService.getUserById(req.params.id);

    res.send({
      success: true,
      data: studentData,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error getting user with this id",
    });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

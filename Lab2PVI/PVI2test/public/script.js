const students = [];

document.addEventListener("DOMContentLoaded", function () {
  let notificationButton = document.getElementById("notification-button");
  let modalMessage = document.querySelector(".modal__masage");
  let accountButton = document.querySelector(".profile-info-button-class");
  let accountModal = document.querySelector(".modal__account");

  notificationButton.addEventListener("mouseenter", function () {
    modalMessage.style.display = "block";
    modalMessage.style.zIndex = "100";
    // Закрити інше модальне вікно, якщо воно відкрите
    accountModal.style.display = "none";
  });

  accountButton.addEventListener("mouseenter", function () {
    accountModal.style.display = "block";
    // Закрити інше модальне вікно, якщо воно відкрите
    modalMessage.style.display = "none";
  });

  // Сховати модальне вікно, коли курсор покидає кнопку
  notificationButton.addEventListener("mouseleave", function () {
    // Перевірка чи курсор знаходиться в області модального вікна
    modalMessage.addEventListener("mouseleave", function (event) {
      let rect = modalMessage.getBoundingClientRect();
      if (
        event.clientX < rect.left ||
        event.clientX > rect.right ||
        event.clientY < rect.top ||
        event.clientY > rect.bottom
      ) {
        // Курсор поза областю модального вікна - сховати його
        modalMessage.style.display = "none";
      }
    });
  });

  // Сховати модальне вікно, коли курсор покидає кнопку
  accountButton.addEventListener("mouseleave", function () {
    // Перевірка чи курсор знаходиться в області модального вікна
    accountModal.addEventListener("mouseleave", function (event) {
      let rect = accountModal.getBoundingClientRect();
      if (
        event.clientX < rect.left ||
        event.clientX > rect.right ||
        event.clientY < rect.top ||
        event.clientY > rect.bottom
      ) {
        // Курсор поза областю модального вікна - сховати його
        accountModal.style.display = "none";
      }
    });
  });
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Function to set up delete button event listeners
function setupDeleteButtonListeners() {
  let allDeleteButtons = document.querySelectorAll(".delete-students-table-button");
  allDeleteButtons.forEach(function (button) {
    button.addEventListener("click", function (event) {
      let clickedButton = event.target;
      let row = clickedButton.closest("tr"); // Отримуємо поточний рядок таблиці
      let nameDisplay = row.querySelector("td:nth-child(3)").textContent.trim(); // Отримуємо ім'я для видалення
      let modalDeleteWindow = document.querySelector(".warning__page");
      let modalMainText = modalDeleteWindow.querySelector(".warning__main");

      modalMainText.textContent = "Are you sure you want to delete " + nameDisplay + " ?";

      modalDeleteWindow.style.display = "block";

      let modalConfirmDeleteButton = modalDeleteWindow.querySelector(".confirm__warning");

      modalConfirmDeleteButton.addEventListener("click", () => {
        row.remove(); // Видаляємо рядок з таблиці
        modalDeleteWindow.style.display = "none";
      });

      let XModalButton = modalDeleteWindow.querySelector(".close-button");
      XModalButton.addEventListener("click", () => {
        modalDeleteWindow.style.display = "none";
      });

      let cancelModalWindowButton = modalDeleteWindow.querySelector(".cancel-button");
      cancelModalWindowButton.addEventListener("click", () => {
        modalDeleteWindow.style.display = "none";
      });
    });
  });
}

function setupEditButtonListeners() {
  let editStudentsButton = document.querySelectorAll(".edit-students-table-button");
  editStudentsButton.forEach((button) => {
    button.addEventListener("click", () => {
      document.querySelector(".edit-page").style.display = "block";
      const row = button.closest("tr");
      showEditModal(row);

      document
        .querySelector(".edit-page .editing-footer .close-button")
        .addEventListener("click", () => {
          document.querySelector(".edit-page").style.display = "none";
        });

      document
        .querySelector(".edit-page .edit-block .edit-header .close-button")
        .addEventListener("click", () => {
          document.querySelector(".edit-page").style.display = "none";
        });
    });
  });
}

// Викликаємо функції для встановлення обробників подій
setupDeleteButtonListeners();
setupEditButtonListeners();

document.querySelector("#students-add-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "flex";
  // Clear input fields after adding the new row
  document.querySelector(".group__input").value = "";
  document.querySelector(".fname__input").value = "";
  document.querySelector(".lname__input").value = "";
  document.querySelector(".gender__input").value = "";
  document.querySelector(".birthday__input").value = "";
});

document.querySelector(".confirm__adding").addEventListener("click", addFunction);

document.querySelector(".adding__footer .close-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "none";
});

document.querySelector(".adding__header .close-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "none";
});


//adding function
function addFunction(event) {
  event.preventDefault();
  function hasNumbers(inputString) {
    return /\d/.test(inputString);
  }

// Check if any of the input fields are empty
  let groupInputValue = document.querySelector(".group__input").value;
  let firstNameInputValue = document.querySelector(".fname__input").value;
  let lastNameInputValue = document.querySelector(".lname__input").value;
  let genderInputValue = document.querySelector(".gender__input").value;

  const hiddenInputValue = document.querySelector("#hidden-input").value;

  let birthdayInputValue = document
      .querySelector(".birthday__input")
      .value.split("-")
      .reverse()
      .join(".");

      let student = {
        id: length+1,
        group: groupInputValue,
        firstName: firstNameInputValue,
        lastName: lastNameInputValue,
        gender: genderInputValue,
        bdayDate: birthdayInputValue
      }

    

  let jsonData = JSON.stringify(student);
  console.log("Script json",jsonData)

  
  // Re-setup delete button event listeners after adding a new row
  setupDeleteButtonListeners();
  // Setup edit button event listeners after adding a new row
  setupEditButtonListeners();
  initCheckboxes();

  fetch("http://localhost:8080/req/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: jsonData,
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("Response from server:", data);
      if(data.statusCode == 201){
        //hode modal
        //add to table
      }else if(data.report){
        //warnings
      }
    })
    .catch((error) => {
      console.log("Помилка:", error);
    });
}



//Edit

// Функція, яка відображає модальне вікно редагування з відповідними даними
function showEditModal(row) {
  // Отримуємо дані з комірок рядка таблиці
  const cells = row.querySelectorAll("td");
  const group = cells[1].textContent;
  const name = cells[2].textContent;
  const gender = cells[3].textContent;
  const birthday = cells[4].textContent.trim(); // Обрізаємо зайві пробіли

  // Встановлюємо значення полів у модальному вікні
  document.querySelector(".group-input").value = group;
  document.querySelector(".fname-input").value = name.split(" ")[0];
  document.querySelector(".lname-input").value = name.split(" ")[1];
  document.querySelector(".gender-input").value = gender;

  document.querySelector(".birthday__input").value = birthday;

  // Отримуємо кнопку підтвердження редагування
  const editButton = document.querySelector(".confirm-editing");

  // Додаємо обробник події для кнопки підтвердження редагування
  editButton.addEventListener("click", () => {
    // Отримуємо нові значення з полів модального вікна
    const newGroup = document.querySelector(".group-input").value;
    const newFirstName = document.querySelector(".fname-input").value;
    const newLastName = document.querySelector(".lname-input").value;
    const newGender = document.querySelector(".gender-input").value;
    let dateCopyy = document.querySelector(".edit-page .birthday__input").value;
    const newBirthday = document
      .querySelector(".edit-page .birthday__input")
      .value.split("-")
      .reverse()
      .join(".");

    let dataCheck = new Date(dateCopyy);

    let date = new Date();

    if (dataCheck > date) {
      Toastify({
        text: "Man, you're living in the future!",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }

    if (
      newGroup === "" ||
      newFirstName === "" ||
      newLastName === "" ||
      newGender === "" ||
      newBirthday === ""
    ) {
      Toastify({
        text: "Fill all fields!",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }

    // Оновлюємо дані у відповідних комірках рядка таблиці
    cells[1].textContent = newGroup;
    cells[2].textContent = newFirstName + " " + newLastName;
    cells[3].textContent = newGender;

    cells[4].textContent = newBirthday;

    // Ховаємо модальне вікно після підтвердження редагування
    document.querySelector(".edit-page").style.display = "none";

    const student = {
      id: students.length + 1,
      group: newGroup,
      firstName: newFirstName,
      lastName: newLastName,
      gender: newGender,
      bdayDate: newBirthday,
      //   hiddenVal: hiddenInputValue,
    };

    fetch("http://localhost:8080/req/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(student),
    })
      .then((response) => {
        if(response.status == 200)
        return response.json()
      })
      .then((data) => {
        console.log("Response from server:", data);
      })
      .catch((error) => {
        console.log("Error:", error);
      });
  });
}

function initCheckboxes() {
  const rows = document.querySelectorAll("table tr");

  rows.forEach((row) => {
    const checkbox = row.querySelector("input[type='checkbox']");
    checkbox.addEventListener("change", () => {
      if (checkbox.checked) {
        row.classList.add("active");
      } else {
        row.classList.remove("active");
      }
    });
  });
}

function mainCheckbox() {
  const checkbox = document.querySelector("table th input[type='checkbox']");
  const otherCheckboxes = document.querySelectorAll("table tbody input[type='checkbox']");

  checkbox.addEventListener("change", () => {
    otherCheckboxes.forEach((check) => {
      check.checked = checkbox.checked;

      const event = new Event("change");
      check.dispatchEvent(event);
    });
  });
}
mainCheckbox();
initCheckboxes();

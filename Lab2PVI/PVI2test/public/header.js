document.addEventListener("DOMContentLoaded", function() {
    let notificationButton = document.getElementById("notification-button");
    let modalMessage = document.querySelector(".modal__masage");
    let accountButton = document.querySelector(".profile-info-button-class");
    let accountModal = document.querySelector(".modal__account");

    notificationButton.addEventListener("mouseenter", function() {
        modalMessage.style.display = "block";
        modalMessage.style.zIndex = "100";
        // Закрити інше модальне вікно, якщо воно відкрите
        accountModal.style.display = "none";
    });

    accountButton.addEventListener("mouseenter", function() {
        accountModal.style.display = "block";
        // Закрити інше модальне вікно, якщо воно відкрите
        modalMessage.style.display = "none";
    });

    // Сховати модальне вікно, коли курсор покидає кнопку
    notificationButton.addEventListener("mouseleave", function() {
        // Перевірка чи курсор знаходиться в області модального вікна
        modalMessage.addEventListener("mouseleave", function(event) {
            let rect = modalMessage.getBoundingClientRect();
            if (event.clientX < rect.left || event.clientX > rect.right || event.clientY < rect.top || event.clientY > rect.bottom) {
                // Курсор поза областю модального вікна - сховати його
                modalMessage.style.display = "none";
            }
        });
    });

    // Сховати модальне вікно, коли курсор покидає кнопку
    accountButton.addEventListener("mouseleave", function() {
        // Перевірка чи курсор знаходиться в області модального вікна
        accountModal.addEventListener("mouseleave", function(event) {
            let rect = accountModal.getBoundingClientRect();
            if (event.clientX < rect.left || event.clientX > rect.right || event.clientY < rect.top || event.clientY > rect.bottom) {
                // Курсор поза областю модального вікна - сховати його
                accountModal.style.display = "none";
            }
        });
    });
});

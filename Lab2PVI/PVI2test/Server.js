
const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')



const app = express()
app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(express.static('public'))
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({
  extended: true
}));

const users = [
  {
    id: "1",
    name: "Vitalii",
    group: "PZ-25",
    gender: "M",
    birthday: "01.01.2000"
  },
  {
    id: "2",
    name: "Vova",
    group: "PZ-26",
    gender: "M",
    birthday: "01.01.2000"
  },
  {
    id: "3",
    name: "Max",
    group: "PZ-27",
    gender: "M",
    birthday: "01.01.2000"
  },


]

app.get('/', (req, res) => {
  res.render('index')
})


app.get('/user', (req, res) => {
  let user = "Not found";
  for (let us of users) {
    if (us.id == req.query.id) {
      user = {};
      for (let key in us) {
        user[key] = us[key];
      }
      break;
    }
    if (us.gender == req.query.gender) {
      user = {};
      for (let key in us) {
        user[key] = us[key];
      }
      break;
    }
  }
  res.send(user);
});


app.get('/user/:id', (req, res) => {
  let user = "Not found"
  for (let us of users) {
    if (us.id == req.params.id) {
      user = {}
      for (let key in us) {
        user[key] = us[key]
      }
      break
    }
  }
  res.send(user)
})

function isValidStudentData(data = {}) {
  const fullAge = (new Date() - new Date(data.bdayDate)) / 1000 / 3600 / 24 / 365

    const result = {
        value: true,
        report: {
            ageRep: fullAge > 16,
            fnameRep: (/^[a-zA-Z]+$/.test(data.firstName)),
            lnameRep: (/^[a-zA-Z]+$/.test(data.lastName))
        }
    }
    for (rep in result.report) {
        result.value &= result.report[rep]
    }

    return result

}

const userScheme = {
  id: Number,
  group: String,
  firstName: String,
  lastName: String,
  gender: String,
  bdayDate: String,
  hiddenVal: String
}

app.post('/req', (req, res) => {

    const studentData = req.body;
    
    console.log("blabla",)
    const isValid = isValidStudentData(studentData)

  

    
    
  
    if (isValid) {
      console.log("Student was added!");
      res.status = 201
      
      res.status(201).json({ message: 'Дані успішно отримано!', status: res.statusCode });
    } else {
      console.log("Invalid student data!");
      res.status(400).json({ message: 'Дані не відповідають вимогам!', status: res.statusCode });
    }
})



function hasNumbers(inputString) {
  return /\d/.test(inputString);
}



app.get('/tasks', (req, res) => {
  res.render('tasks')
})

const PORT = 8080

app.listen(PORT, () => {
  console.log(`Opened: http://localhost:${PORT}`)
})



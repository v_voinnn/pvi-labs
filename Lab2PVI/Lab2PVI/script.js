const students = [];

document.addEventListener("DOMContentLoaded", function () {
  let notificationButton = document.getElementById("notification-button");
  let modalMessage = document.querySelector(".modal__masage");
  let accountButton = document.querySelector(".profile-info-button-class");
  let accountModal = document.querySelector(".modal__account");

  notificationButton.addEventListener("mouseenter", function () {
    modalMessage.style.display = "block";
    modalMessage.style.zIndex = "100";
    // Закрити інше модальне вікно, якщо воно відкрите
    accountModal.style.display = "none";
  });

  accountButton.addEventListener("mouseenter", function () {
    accountModal.style.display = "block";
    // Закрити інше модальне вікно, якщо воно відкрите
    modalMessage.style.display = "none";
  });

  // Сховати модальне вікно, коли курсор покидає кнопку
  notificationButton.addEventListener("mouseleave", function () {
    // Перевірка чи курсор знаходиться в області модального вікна
    modalMessage.addEventListener("mouseleave", function (event) {
      let rect = modalMessage.getBoundingClientRect();
      if (
        event.clientX < rect.left ||
        event.clientX > rect.right ||
        event.clientY < rect.top ||
        event.clientY > rect.bottom
      ) {
        // Курсор поза областю модального вікна - сховати його
        modalMessage.style.display = "none";
      }
    });
  });

  // Сховати модальне вікно, коли курсор покидає кнопку
  accountButton.addEventListener("mouseleave", function () {
    // Перевірка чи курсор знаходиться в області модального вікна
    accountModal.addEventListener("mouseleave", function (event) {
      let rect = accountModal.getBoundingClientRect();
      if (
        event.clientX < rect.left ||
        event.clientX > rect.right ||
        event.clientY < rect.top ||
        event.clientY > rect.bottom
      ) {
        // Курсор поза областю модального вікна - сховати його
        accountModal.style.display = "none";
      }
    });
  });
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Function to set up delete button event listeners
function setupDeleteButtonListeners() {
  let allDeleteButtons = document.querySelectorAll(".delete-students-table-button");
  allDeleteButtons.forEach(function (button) {
    button.addEventListener("click", function (event) {
      let clickedButton = event.target;
      let row = clickedButton.closest("tr"); // Отримуємо поточний рядок таблиці
      let nameDisplay = row.querySelector("td:nth-child(3)").textContent.trim(); // Отримуємо ім'я для видалення
      let modalDeleteWindow = document.querySelector(".warning__page");
      let modalMainText = modalDeleteWindow.querySelector(".warning__main");

      modalMainText.textContent = "Are you sure you want to delete " + nameDisplay + " ?";

      modalDeleteWindow.style.display = "block";

      let modalConfirmDeleteButton = modalDeleteWindow.querySelector(".confirm__warning");

      modalConfirmDeleteButton.addEventListener("click", () => {
        row.remove(); // Видаляємо рядок з таблиці
        modalDeleteWindow.style.display = "none";
      });

      let XModalButton = modalDeleteWindow.querySelector(".close-button");
      XModalButton.addEventListener("click", () => {
        modalDeleteWindow.style.display = "none";
      });

      let cancelModalWindowButton = modalDeleteWindow.querySelector(".cancel-button");
      cancelModalWindowButton.addEventListener("click", () => {
        modalDeleteWindow.style.display = "none";
      });
    });
  });
}

function setupEditButtonListeners() {
  let editStudentsButton = document.querySelectorAll(".edit-students-table-button");
  editStudentsButton.forEach((button) => {
    button.addEventListener("click", () => {
      document.querySelector(".edit-page").style.display = "block";
      const row = button.closest("tr");
      showEditModal(row);

      document
        .querySelector(".edit-page .editing-footer .close-button")
        .addEventListener("click", () => {
          document.querySelector(".edit-page").style.display = "none";
        });

      document
        .querySelector(".edit-page .edit-block .edit-header .close-button")
        .addEventListener("click", () => {
          document.querySelector(".edit-page").style.display = "none";
        });
    });
  });
}

// Викликаємо функції для встановлення обробників подій
setupDeleteButtonListeners();
setupEditButtonListeners();

document.querySelector("#students-add-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "flex";
  // Clear input fields after adding the new row
  document.querySelector(".group__input").value = "";
  document.querySelector(".fname__input").value = "";
  document.querySelector(".lname__input").value = "";
  document.querySelector(".gender__input").value = "";
  document.querySelector(".birthday__input").value = "";
});

document.querySelector(".confirm__adding").addEventListener("click", addFunction);

document.querySelector(".adding__footer .close-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "none";
});

document.querySelector(".adding__header .close-button").addEventListener("click", () => {
  document.querySelector(".adding__page").style.display = "none";
});

function addFunction() {
  // Check if any of the input fields are empty
  function hasNumbers(inputString) {
    return /\d/.test(inputString);
  }

  let groupInputValue = document.querySelector(".group__input").value;
  let firstNameInputValue = document.querySelector(".fname__input").value;
  let lastNameInputValue = document.querySelector(".lname__input").value;
  let genderInputValue = document.querySelector(".gender__input").value;

  let dateCopy = document.querySelector(".birthday__input").value;
  let datacheck = new Date(dateCopy);

  const hiddenInputValue = document.querySelector("#hidden-input").value;

  let birthdayInputValue = document
      .querySelector(".birthday__input")
      .value.split("-")
      .reverse()
      .join(".");
  let date = new Date();

  if (datacheck > date) {
    Toastify({
      text: "Man, you're living in the future!",
      duration: 2000,
      className: "info",
      style: {
        background: "red",
      },
    }).showToast();
    return;
  }

  if (
      groupInputValue === "" ||
      firstNameInputValue === "" ||
      lastNameInputValue === "" ||
      genderInputValue === "" ||
      birthdayInputValue === ""
  ) {
    Toastify({
      text: "Fill all fields!",
      duration: 2000,
      className: "info",
      style: {
        background: "red",
      },
    }).showToast();
    return;
  }

  if (hasNumbers(firstNameInputValue) || hasNumbers(lastNameInputValue)) {
    Toastify({
      text: "Invalid name and/or surname! Names should not contain numbers.",
      duration: 2000,
      className: "info",
      style: {
        background: "red",
      },
    }).showToast();
    return;
  }

  const currentDate = new Date();
  const minDate = new Date(currentDate.getFullYear() - 70, currentDate.getMonth(), currentDate.getDate());
  const maxDate = new Date(currentDate.getFullYear() - 16, currentDate.getMonth(), currentDate.getDate());

  if (datacheck < minDate || datacheck > maxDate) {
    Toastify({
      text: "Invalid age! Age should be between 16 and 70.",
      duration: 2000,
      className: "info",
      style: {
        background: "red",
      },
    }).showToast();
    return;
  }

  const student = {
    id: students.length + 1,
    group: groupInputValue,
    firstName: firstNameInputValue,
    lastName: lastNameInputValue,
    gender: genderInputValue,
    bdayDate: birthdayInputValue,
    hiddenVal: hiddenInputValue,
  };

  const existingStudent = students.find(
      (student) =>
          student.firstName === firstNameInputValue && student.lastName === lastNameInputValue
  );

  if (existingStudent) {
    Toastify({
      text: "You already have such student!",
      duration: 2000,
      className: "info",
      style: {
        background: "red",
      },
    }).showToast();
    return;
  } else {
    students.push(student);
  }


  let newRow = document.createElement("tr");
  newRow.classList.add("table-row");
  newRow.innerHTML = `
        <td><input type="checkbox"></td>
        <td>${groupInputValue}</td>
        <td>${firstNameInputValue} ${lastNameInputValue}</td>
        <td>${genderInputValue}</td>
        <td>${birthdayInputValue}</td>
        <td class="status-cell"><div class="status-circle-indicator"></div></td>
        <td>
            <button class="edit-students-table-button">
                <svg xmlns="http://www.w3.org/2000/svg" width="1.3em" height="1.3em" viewBox="0 0 24 24">
                    <path fill="none" stroke="currentColor" stroke-width="1.5" d="m14.36 4.079l.927-.927a3.932 3.932 0 0 1 5.561 5.561l-.927.927m-5.56-5.561s.115 1.97 1.853 3.707C17.952 9.524 19.92 9.64 19.92 9.64m-5.56-5.561l-8.522 8.52c-.577.578-.866.867-1.114 1.185a6.556 6.556 0 0 0-.749 1.211c-.173.364-.302.752-.56 1.526l-1.094 3.281m17.6-10.162L11.4 18.16c-.577.577-.866.866-1.184 1.114a6.554 6.554 0 0 1-1.211.749c-.364.173-.751.302-1.526.56l-3.281 1.094m0 0l-.802.268a1.06 1.06 0 0 1-1.342-1.342l.268-.802m1.876 1.876l-1.876-1.876" />
                </svg>
            </button>
            <button class="delete-students-table-button">
                <svg xmlns="http://www.w3.org/2000/svg" width="1.3em" height="1.3em" viewBox="0 0 14 14">
                    <path fill="currentColor" fill-rule="evenodd" d="M1.707.293A1 1 0 0 0 .293 1.707L5.586 7L.293 12.293a1 1 0 1 0 1.414 1.414L7 8.414l5.293 5.293a1 1 0 0 0 1.414-1.414L8.414 7l5.293-5.293A1 1 0 0 0 12.293.293L7 5.586z" clip-rule="evenodd" />
                </svg>
            </button>
        </td>
    `;

  newRow.className = "table-row"; // Add a class to the new row
  document.querySelector(".scrollable-table").appendChild(newRow);

  let jsonData = JSON.stringify(student);

  console.log("New student:", jsonData);
  console.log("Array of students", students);

  // Hide the modal
  document.querySelector(".adding__page").style.display = "none";

  // Re-setup delete button event listeners after adding a new row
  setupDeleteButtonListeners();
  // Setup edit button event listeners after adding a new row
  setupEditButtonListeners();
  initCheckboxes();

  fetch("http://localhost:8080/req/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: jsonData,
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("Response from server:", data);
    })
    .catch((error) => {
      console.log("Error:", error);
    });
}

//Edit

// Функція, яка відображає модальне вікно редагування з відповідними даними
function showEditModal(row) {

  function hasNumbers(inputString) {
    return /\d/.test(inputString);
  }

  // Отримуємо дані з комірок рядка таблиці
  const cells = row.querySelectorAll("td");
  const group = cells[1].textContent;
  const name = cells[2].textContent;
  const gender = cells[3].textContent;
  const birthday = cells[4].textContent.trim(); // Обрізаємо зайві пробіли

  // Встановлюємо значення полів у модальному вікні
  document.querySelector(".group-input").value = group;
  document.querySelector(".fname-input").value = name.split(" ")[0];
  document.querySelector(".lname-input").value = name.split(" ")[1];
  document.querySelector(".gender-input").value = gender;

  document.querySelector(".birthday__input").value = birthday;

  // Отримуємо кнопку підтвердження редагування
  const editButton = document.querySelector(".confirm-editing");

  // Додаємо обробник події для кнопки підтвердження редагування
  editButton.addEventListener("click", () => {
    // Отримуємо нові значення з полів модального вікна
    const newGroup = document.querySelector(".group-input").value;
    const newFirstName = document.querySelector(".fname-input").value;
    const newLastName = document.querySelector(".lname-input").value;
    const newGender = document.querySelector(".gender-input").value;
    let dateCopyy = document.querySelector(".edit-page .birthday__input").value;
    const newBirthday = document
      .querySelector(".edit-page .birthday__input")
      .value.split("-")
      .reverse()
      .join(".");

    let dataCheck = new Date(dateCopyy);

    let date = new Date();


    if (
        newGroup === "" ||
        newFirstName === "" ||
        newLastName === "" ||
        newGender === "" ||
        newBirthday === ""
    ) {
      Toastify({
        text: "Fill all fields!",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }

    if (dataCheck > date) {
      Toastify({
        text: "Man, you're living in the future!",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }

    if (hasNumbers(newFirstName) || hasNumbers(newLastName)) {
      Toastify({
        text: "Invalid name and/or surname! Names should not contain numbers.",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }


    let datacheck = new Date(dateCopyy);


    const currentDate = new Date();
    const minDate = new Date(currentDate.getFullYear() - 70, currentDate.getMonth(), currentDate.getDate());
    const maxDate = new Date(currentDate.getFullYear() - 16, currentDate.getMonth(), currentDate.getDate());

    if (datacheck < minDate || datacheck > maxDate) {
      Toastify({
        text: "Invalid age! Age should be between 16 and 70.",
        duration: 2000,
        className: "info",
        style: {
          background: "red",
        },
      }).showToast();
      return;
    }

    // Оновлюємо дані у відповідних комірках рядка таблиці
    cells[1].textContent = newGroup;
    cells[2].textContent = newFirstName + " " + newLastName;
    cells[3].textContent = newGender;

    cells[4].textContent = newBirthday;

    // Ховаємо модальне вікно після підтвердження редагування
    document.querySelector(".edit-page").style.display = "none";

    const student = {
      id: students.length + 1,
      group: newGroup,
      firstName: newFirstName,
      lastName: newLastName,
      gender: newGender,
      bdayDate: newBirthday,
      //   hiddenVal: hiddenInputValue,
    };

    fetch("http://localhost:8080/req/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(student),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Response from server:", data);
      })
      .catch((error) => {
        console.log("Error:", error);
      });
  });
}

function initCheckboxes() {
  const rows = document.querySelectorAll("table tr");

  rows.forEach((row) => {
    const checkbox = row.querySelector("input[type='checkbox']");
    checkbox.addEventListener("change", () => {
      if (checkbox.checked) {
        row.classList.add("active");
      } else {
        row.classList.remove("active");
      }
    });
  });
}

function mainCheckbox() {
  const checkbox = document.querySelector("table th input[type='checkbox']");
  const otherCheckboxes = document.querySelectorAll("table tbody input[type='checkbox']");

  checkbox.addEventListener("change", () => {
    otherCheckboxes.forEach((check) => {
      check.checked = checkbox.checked;

      const event = new Event("change");
      check.dispatchEvent(event);
    });
  });
}
mainCheckbox();
initCheckboxes();

"use client";

import { Bell, Frame } from "lucide-react";
import { Avatar, AvatarFallback, AvatarImage } from "../ui/avatar";
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import { Button } from "../ui/button";
import Link from "next/link";
import { Badge } from "../ui/badge";
import { useContext } from "react";
import { AuthContext } from "../providers/AuthContext";

export const Header = () => {
    const { username, logout } = useContext(AuthContext);

    return (
        <header className="flex justify-between items-center py-4">
            <Link href="/" className="flex items-center gap-2">
                <p className="font-bold">CMS</p>
            </Link>
            <nav className="flex gap-4">
                <div className="flex items-center gap-4">
                    <DropdownMenu>
                        <DropdownMenuTrigger asChild>
                            <Button
                                variant="outline"
                                className="group relative bg-slate-200"
                            >
                                <Bell className="group-hover:scale-110 transition-transform animate-bounce" />
                                <div className="w-3 h-3 rounded-full animate-ping absolute top-1 right-2 bg-red-500" />
                            </Button>
                        </DropdownMenuTrigger>
                        <DropdownMenuContent className="w-56">
                            <DropdownMenuItem>
                                <div className="flex justify-between items-center w-full">
                                    <span>Notifications</span>
                                    <Badge>3</Badge>
                                </div>
                            </DropdownMenuItem>
                        </DropdownMenuContent>
                    </DropdownMenu>
                    {!!username && (
                        <DropdownMenu>
                            <DropdownMenuTrigger asChild>
                                <div className="flex gap-4 items-center cursor-pointer hover:bg-slate-100 p-2 rounded-md">
                                    <Avatar>
                                        <AvatarImage
                                            src="https://pngimg.com/uploads/lemur/lemur_PNG34.png"
                                            alt="@shadcn"
                                        />
                                        <AvatarFallback>CN</AvatarFallback>
                                    </Avatar>
                                    <p>{username}</p>
                                </div>
                            </DropdownMenuTrigger>
                            <DropdownMenuContent className="w-40">
                                <DropdownMenuItem
                                    onClick={() => {
                                        logout();
                                    }}
                                >
                                    Log out
                                </DropdownMenuItem>
                            </DropdownMenuContent>
                        </DropdownMenu>
                    )}
                    {!username && (
                        <>
                            <Button>
                                <Link href="/register">Sign Up</Link>
                            </Button>
                            <Button>
                                <Link href="/login">Login</Link>
                            </Button>

                        </>
                    )}
                </div>
            </nav>
        </header>
    );
};

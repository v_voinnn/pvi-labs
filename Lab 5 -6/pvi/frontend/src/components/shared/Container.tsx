import { cn } from "@/lib/utils";
import { ComponentPropsWithRef, FC } from "react";

interface ContainerPropsType extends ComponentPropsWithRef<"div"> {}

export const Container: FC<ContainerPropsType> = ({
    children,
    className,
    ...rest
}) => {
    return (
        <div
            className={cn("max-w-[1500px] w-full px-4 mx-auto", className)}
            {...rest}
        >
            {children}
        </div>
    );
};

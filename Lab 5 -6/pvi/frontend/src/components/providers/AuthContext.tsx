"use client";

import { api } from "@/lib/api";
import { createContext, useState, useEffect } from "react";

interface AuthProviderPropsType {
    children: React.ReactNode;
}

interface AuthContextType {
    isAuthenticated: boolean;
    userId: string;
    username: string;
    login: (id: string) => void;
    logout: () => void;
}

export const AuthContext = createContext<AuthContextType>({
    isAuthenticated: false,
    userId: "-1",
    username: "",
    login: () => {},
    logout: () => {},
});

export const AuthProvider: React.FC<AuthProviderPropsType> = ({ children }) => {
    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
    const [userId, setUserId] = useState<string>(
        localStorage.getItem("userId") ?? "-1"
    );
    const [username, setUsername] = useState(
        localStorage.getItem("username") ?? ""
    );

    useEffect(() => {
        // Перевірка локального сховища під час завантаження компонента
        const storedAuth = localStorage.getItem("isAuthenticated");
        if (storedAuth) {
            setIsAuthenticated(!!JSON.parse(storedAuth));
        }
    }, []);

    const login = async (id) => {
        setUserId(id);
        setIsAuthenticated(true);
        localStorage.setItem("isAuthenticated", "1");
        localStorage.setItem("userId", id);

        const userData = await api.get("/auth/" + id).then((data) => data.data);
        setUsername(userData.username);
        localStorage.setItem("username", userData.username);
    };

    const logout = () => {
        // Логіка виходу з авторизації тут
        setIsAuthenticated(false);
        setUserId("-1");
        setUsername("");
        localStorage.setItem("userId", "-1");
        localStorage.removeItem("isAuthenticated");
        localStorage.setItem("username", "");

        if (typeof window !== undefined) {
            window.location.href = "/login";
        }
    };

    return (
        <AuthContext.Provider
            value={{ isAuthenticated, userId, login, logout, username }}
        >
            {children}
        </AuthContext.Provider>
    );
};

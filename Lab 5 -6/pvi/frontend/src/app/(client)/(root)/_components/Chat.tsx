"use client";

import { AuthContext } from "@/components/providers/AuthContext";
import { Dialog, DialogContent } from "@/components/ui/dialog";
import React, { useContext, useEffect, useState } from "react";
import { Container, Dropdown, Button, Modal } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { Socket } from "socket.io-client";

interface ChatPropsType {
    socket: Socket;
    room: any;
}

export const Chat: React.FC<ChatPropsType> = ({ socket, room }) => {
    const [currentMessage, setCurrentMessage] = useState("");
    const { register, handleSubmit } = useForm();
    const [username, setUsername] = useState<string>("");
    const [messageList, setMessageList] = useState<any[]>([]);
    const [currentRoom, setCurrentRoom] = useState<any>(null);
    const [show, setShow] = useState(false);

    const { userId } = useContext(AuthContext);

    const modalClose = () => {
        setShow(false);
    };
    const modalShow = () => setShow(true);

    fetch("http://localhost:3001/auth/" + userId)
        .then((response) => response.json())
        .then((data) => {
            setUsername(data.username);
        });
    const sendMessage = async () => {
        if (currentMessage === "" || !username) return;
        const messageData = {
            authorId: userId,
            room: room,
            author: username,
            message: currentMessage,
            date:
                new Date(Date.now()).getHours() +
                ":" +
                new Date(Date.now()).getMinutes(),
        };
        setCurrentMessage("");
        await socket.emit("send_message", messageData);
    };

    useEffect(() => {
        socket.on("recieve_message", (data) => {
            setMessageList((list) => [...list, data]);
        });
    }, [socket]);

    useEffect(() => {
        fetch("http://localhost:3001/chat/oneRoom/" + room)
            .then((response) => {
                if (response) return response.json();
            })
            .then((data) => {
                console.log(data);
                if (data) {
                    setCurrentRoom(data);
                    setMessageList(() => data.messages);
                }
            });
    }, []);

    const addMember = (data) => {
        const dataToSend = {
            room: currentRoom._id,
            username: data.secondMemberUsername,
        };
        fetch("http://localhost:3001/chat/addUser/", {
            method: "POST",
            mode: "cors",
            body: JSON.stringify(dataToSend),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        })
            .then((response) => response && response.json())
            .then((data) =>
                data && data.message
                    ? alert(data.message)
                    : window.location.reload()
            );
    };

    return (
        <>
            <Dialog open={show}>
                <DialogContent onInteractOutside={modalClose}>
                    <form onSubmit={handleSubmit(addMember)}>
                        <Modal.Body>
                            <div className="mt-2">
                                <label
                                    htmlFor="secondMember"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Member username
                                </label>
                                <input
                                    type="text"
                                    className="mt-1 block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring focus:ring-opacity-50"
                                    id="secondMember"
                                    {...register("secondMemberUsername")}
                                />
                            </div>
                        </Modal.Body>
                        <Modal.Footer className="p-1">
                            <button
                                className="px-4 py-2 border border-red-500 text-red-500 rounded-md hover:bg-red-50"
                                type="reset"
                                onClick={modalClose}
                            >
                                Cancel
                            </button>
                            <button
                                className="px-4 py-2 border border-gray-800 text-gray-800 rounded-md hover:bg-gray-100"
                                type="submit"
                            >
                                Add member
                            </button>
                        </Modal.Footer>
                    </form>
                </DialogContent>
            </Dialog>
            <div className="card h-[550px] relative">
                <div className="card-header flex justify-between items-center p-3">
                    <h5 className="mb-0">
                        Chat messages
                        {currentRoom && currentRoom.users && (
                            <Dropdown className="absolute right-2 top-2 z-20">
                                <Dropdown.Toggle className="border border-black">
                                    Members
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="bg-white w-[200px]">
                                    <Dropdown.Item
                                        className="pb-2 border-b border-gray-300"
                                        onClick={modalShow}
                                    >
                                        Add member
                                    </Dropdown.Item>
                                    <br />
                                    {currentRoom.users.map((x, index) => (
                                        <Dropdown.Item key={index}>
                                            <div className="p-2 mt-2">
                                                {x.username}
                                                <p className="text-sm text-gray-500">
                                                    {x.firstName} {x.lastName}
                                                </p>
                                            </div>
                                        </Dropdown.Item>
                                    ))}
                                </Dropdown.Menu>
                            </Dropdown>
                        )}
                    </h5>
                </div>
                <div className="card-body relative h-[400px] overflow-y-auto">
                    {messageList &&
                        messageList.map((data, index) =>
                            data.author !== username ? (
                                <div
                                    key={index}
                                    className="flex flex-row justify-start mb-4"
                                >
                                    <div>
                                        <p className="text-sm mb-0">
                                            {data.author}
                                        </p>
                                        <p className="bg-gray-100 p-2 mb-1 rounded-md">
                                            {data.message}
                                        </p>
                                        <p className="text-sm text-gray-500 mb-3">
                                            {data.date}
                                        </p>
                                    </div>
                                </div>
                            ) : (
                                <div
                                    key={index}
                                    className="flex flex-row justify-end"
                                >
                                    <div>
                                        <p className="text-sm text-right mb-0">
                                            {data.author}
                                        </p>
                                        <p className="bg-blue-500 text-white p-2 mb-1 rounded-md">
                                            {data.message}
                                        </p>
                                        <p className="text-sm text-gray-500 text-right mb-3">
                                            {data.date}
                                        </p>
                                    </div>
                                </div>
                            )
                        )}
                </div>
                <div className="card-footer flex w-full">
                    <div className="input-group mb-3 flex w-full">
                        <input
                            type="text"
                            className="form-control flex-1"
                            placeholder="Enter your message"
                            value={currentMessage}
                            onChange={(e) => setCurrentMessage(e.target.value)}
                        />
                        <button
                            className="px-4 py-2 border border-gray-800 text-gray-800 rounded-md hover:bg-gray-100"
                            type="button"
                            onClick={sendMessage}
                        >
                            Send
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

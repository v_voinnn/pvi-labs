"use client";

import React, { useState, useEffect, useContext } from "react";
import { io } from "socket.io-client";
import { Chat } from "./_components/Chat";
import { useForm } from "react-hook-form";
import { useSearchParams } from "next/navigation";

import { AuthContext } from "@/components/providers/AuthContext";

export default function Home() {
    const { register, handleSubmit, setValue } = useForm();
    // @ts-ignore
    const socket = io.connect("http://localhost:3001");
    const searchParams = useSearchParams();

    const { userId } = useContext(AuthContext);

    const joinRoom = async (room) => {
        await socket.emit("join_room", room);
    };

    const [room, setRoom] = useState<any>(searchParams.get("room"));

    useEffect(() => {
        joinRoom(room);
    }, [room]);

    const [chatRooms, setChatRooms] = useState<any[]>([]);
    const fetchChatRooms = () => {
        fetch("http://localhost:3001/chat/rooms/" + userId)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                setChatRooms(data);
                console.log(
                    chatRooms.length > 0 ? chatRooms : "Дані ще не завантажені"
                );
            });
    };
    useEffect(() => {
        fetchChatRooms();
    }, []);

    const createRoom = (formData) => {
        fetch(
            "http://localhost:3001/auth/" + userId + "/" + formData.secondMember
        )
            .then((response) => {
                console.log(response);
                if (response) return response.json();
            })
            .then((data) => {
                console.log(data);
                if (data.message) throw new Error("User not found");
                return fetch("http://localhost:3001/chat/rooms", {
                    method: "POST",
                    mode: "cors",
                    body: JSON.stringify({
                        users: data,
                        name: formData.roomName,
                    }),
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                });
            })
            .then((response) => {
                return response.json();
            })
            .then(
                (roomdata) => (window.location.href = "?room=" + roomdata._id)
            )
            .catch((err) => alert(err.message));
    };

    const [isFormVisible, setFormVisible] = useState(false);

    return (
        <>
            <form id="addRoomForm" onSubmit={handleSubmit(createRoom)}>
                <div
                    className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center z-50"
                    id="add-student-overlay"
                    style={{ display: isFormVisible ? "flex" : "none" }}
                >
                    <div className="bg-white rounded-lg shadow-lg w-1/2">
                        <h5 className="bg-gray-100 p-4 rounded-t-lg">
                            Add new room
                        </h5>
                        <div className="p-6">
                            <div className="mb-4">
                                <label
                                    htmlFor="roomName"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Room name
                                </label>
                                <input
                                    type="text"
                                    className="mt-1 block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring focus:ring-opacity-50"
                                    id="roomName"
                                    {...register("roomName")}
                                />
                            </div>
                            <div className="mb-4">
                                <label
                                    htmlFor="secondMember"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Second member
                                </label>
                                <input
                                    type="text"
                                    className="mt-1 block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring focus:ring-opacity-50"
                                    id="secondMember"
                                    {...register("secondMember")}
                                />
                            </div>
                            <div className="flex justify-end space-x-4">
                                <button
                                    type="reset"
                                    className="px-4 py-2 border border-red-500 text-red-500 rounded-md hover:bg-red-50"
                                    onClick={() => setFormVisible(false)}
                                >
                                    Cancel
                                </button>
                                <button
                                    type="submit"
                                    className="px-4 py-2 border border-gray-800 text-gray-800 rounded-md hover:bg-gray-100"
                                >
                                    Ok
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div className="flex mt-4">
                <div className="w-1/4">
                    <div className="bg-white rounded-lg shadow-lg h-96">
                        <div className="flex justify-between items-center p-4 bg-gray-100 rounded-t-lg">
                            <h5 className="mb-0">Chat Rooms</h5>
                            <button
                                className="px-3 py-1 border border-gray-800 text-gray-800 rounded-md hover:bg-gray-100"
                                onClick={() => setFormVisible(true)}
                            >
                                +
                            </button>
                        </div>
                        <div className="overflow-y-auto h-full">
                            <div className="list-none p-0">
                                {chatRooms.length > 0 &&
                                    chatRooms.map((chatRoom, index) => (
                                        <button
                                            key={index}
                                            id={chatRoom._id}
                                            type="button"
                                            className={`block w-full px-4 py-2 text-left border-b border-gray-200 ${
                                                room === chatRoom._id
                                                    ? "bg-gray-200"
                                                    : "bg-white"
                                            } hover:bg-gray-100 focus:outline-none`}
                                            onClick={() => {
                                                window.location.href =
                                                    "?room=" + chatRoom._id;
                                            }}
                                        >
                                            {chatRoom.name}
                                        </button>
                                    ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-1/2 ml-4 p-4">
                    <Chat room={room} socket={socket} />
                </div>
            </div>
        </>
    );
}

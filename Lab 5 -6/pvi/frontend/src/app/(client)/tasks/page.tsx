"use client";

import { useEffect, useState } from "react";
import { TaskTable } from "./_components/TaskTable";
import { api } from "@/lib/api";

const TasksPage = () => {
    const [tasks, setTasks] = useState<any>(null);

    useEffect(() => {
        api.get("/tasks")
            .then((data) =>
                data.data.tasks.map((task) => ({
                    id: task._id,
                    content: task.content,
                    column: task.column,
                }))
            )
            .then((data) => {
                console.log(data);
                const todos = data.filter((item) => item.column === "todo");
                const inProgress = data.filter(
                    (item) => item.column === "inProgress"
                );
                const qaReady = data.filter(
                    (item) => item.column === "qaReady"
                );
                const done = data.filter((item) => item.column === "done");

                setTasks({
                    todo: todos,
                    inProgress,
                    qaReady,
                    done,
                });
            });
    }, []);

    return (
        <div>
            {!!tasks && (
                <TaskTable
                    tasksMap={tasks}
                    columnNames={[
                        "To Do",
                        "In progress",
                        "Testing",
                        "Done!",
                    ]}
                    onChange={() => {}}
                />
            )}
        </div>
    );
};

export default TasksPage;

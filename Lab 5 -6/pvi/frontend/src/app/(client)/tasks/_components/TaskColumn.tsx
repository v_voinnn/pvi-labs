import { Card, CardContent } from "@/components/ui/card";
import { cn } from "@/lib/utils";
import {
    Draggable,
    DraggableProvided,
    Droppable,
    DroppableProvided,
    DroppableStateSnapshot,
} from "@hello-pangea/dnd";
import React from "react";

type Props = {
    listId: string;
    listType: string;
    tasks: Task[];
    title: string;
    isDropDisabled: boolean;
};

type TaskListProps = {
    tasks: Task[];
};

type Task = {
    id: string;
    content: string;
};

const InnerQuoteList = React.memo(function InnerQuoteList(
    props: TaskListProps
) {
    console.log("lox", props.tasks);
    return props.tasks.map((task: Task, index: number) => (
        <Draggable key={task.id} draggableId={task.id} index={index}>
            {(dragProvided: DraggableProvided) => (
                <Card
                    className="rounded-md mt-4"
                    ref={dragProvided.innerRef}
                    {...dragProvided.draggableProps}
                    {...dragProvided.dragHandleProps}
                >
                    <CardContent>{task.content}</CardContent>
                </Card>
            )}
        </Draggable>
    ));
});

type InnerListProps = {
    dropProvided: DroppableProvided;
    tasks: Task[];
    title: string;
};

const InnerList = (props: InnerListProps) => {
    const { tasks, title, dropProvided } = props;

    return (
        <div className="bg-slate-100 p-4 rounded-md">
            <p className="font-bold">{title}</p>
            <div
                className="min-h-[200px] pb-4 flex flex-col"
                ref={dropProvided.innerRef}
            >
                <InnerQuoteList tasks={tasks} />
                {dropProvided.placeholder}
            </div>
        </div>
    );
};

export const TaskList = (props: Props) => {
    const { isDropDisabled, listId = "LIST", listType, tasks, title } = props;

    return (
        <Droppable droppableId={listId} type={listType}>
            {(
                dropProvided: DroppableProvided,
                dropSnapshot: DroppableStateSnapshot
            ) => (
                <div
                    className={cn({
                        "": dropSnapshot.isDraggingOver,
                        "": Boolean(dropSnapshot.draggingFromThisWith),
                        "":
                            !dropSnapshot.isDraggingOver &&
                            !Boolean(dropSnapshot.draggingFromThisWith),
                        "opacity-50": isDropDisabled,
                    })}
                    {...dropProvided.droppableProps}
                >
                    <InnerList
                        tasks={tasks}
                        title={title}
                        dropProvided={dropProvided}
                    />
                </div>
            )}
        </Droppable>
    );
};

import { Button } from "@/components/ui/button";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogFooter,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { api } from "@/lib/api";
import { useRouter } from "next/navigation";
import { useRef } from "react";

interface AddTaskModalPropsType {
    onAddTask: (id: string, content: string) => void;
    onClose: () => void;
    isVisible?: boolean;
}

export const AddTaskModal: React.FC<AddTaskModalPropsType> = ({
    onAddTask,
    isVisible,
    onClose,
}) => {
    const contentValRef = useRef<HTMLTextAreaElement>(null);
    const router = useRouter();

    const addTask = (content: string) => {
        api.post("/tasks", { content }).then((data) => {
            onAddTask(data.data.tasks._id, data.data.tasks.content);
            router.refresh();
        });
    };

    return (
        <Dialog open={isVisible}>
            <DialogContent
                className="sm:max-w-[425px]"
                onInteractOutside={onClose}
            >
                <DialogHeader>
                    <DialogTitle>Create task</DialogTitle>
                    <DialogDescription>
                        Assign task for yourself
                    </DialogDescription>
                </DialogHeader>
                <div className="grid gap-4 py-4">
                    <div className="flex flex-col gap-3">
                        <Label htmlFor="task">Your task content</Label>
                        <Textarea
                            id="task"
                            className="col-span-3"
                            ref={contentValRef}
                            placeholder="Type task content"
                        />
                    </div>
                </div>
                <DialogFooter>
                    <Button
                        onClick={() => {
                            addTask(contentValRef.current?.value ?? "");
                            onClose();
                        }}
                        type="submit"
                    >
                        Create task
                    </Button>
                </DialogFooter>
            </DialogContent>
        </Dialog>
    );
};

import {
    DragDropContext,
    DraggableLocation,
    DropResult,
} from "@hello-pangea/dnd";
import { useState } from "react";
import { TaskList } from "./TaskColumn";
import { AddTaskModal } from "./AddTaskModal";
import { Button } from "@/components/ui/button";
import { Plus } from "lucide-react";
import { api } from "@/lib/api";
import { useRouter } from "next/navigation";

type Task = {
    id: string;
    content: string;
};

interface TaskMap {
    [key: string]: Task[];
}

interface ReorderTaskMapArgs {
    taskMap: TaskMap;
    source: DraggableLocation;
    destination: DraggableLocation;
}

interface ReorderTaskMapResult {
    taskMap: TaskMap;
}

function reorder<TItem>(
    list: TItem[],
    startIndex: number,
    endIndex: number
): TItem[] {
    const result = [...list];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
}

export const reorderTaskMap = ({
    taskMap,
    source,
    destination,
}: ReorderTaskMapArgs): ReorderTaskMapResult => {
    const current: Task[] = [...taskMap[source.droppableId]];
    const next: Task[] = [...taskMap[destination.droppableId]];
    const target: Task = current[source.index];

    // moving to same list
    if (source.droppableId === destination.droppableId) {
        const reordered: Task[] = reorder(
            current,
            source.index,
            destination.index
        );
        const result: TaskMap = {
            ...taskMap,
            [source.droppableId]: reordered,
        };
        return {
            taskMap: result,
        };
    }

    // moving to different list

    // remove from original
    current.splice(source.index, 1);
    // insert into next
    next.splice(destination.index, 0, target);

    const result: TaskMap = {
        ...taskMap,
        [source.droppableId]: current,
        [destination.droppableId]: next,
    };

    return {
        taskMap: result,
    };
};

interface TaskTablePropsType {
    tasksMap: TaskMap;
    onChange: () => void;
    columnNames: string[];
}

export const TaskTable: React.FC<TaskTablePropsType> = ({
    tasksMap,
    onChange,
    columnNames,
}) => {
    const [taskMap, setTaskMap] = useState<{ [key: string]: Task[] }>(tasksMap);
    const [isAddTaskModalVisible, setIsAddTaskModalVisible] = useState(false);
    const router = useRouter();

    const onDragEnd = (result: DropResult): void => {
        // dropped nowhere
        if (!result.destination) {
            return;
        }

        const source: DraggableLocation = result.source;
        const destination: DraggableLocation = result.destination;

        api.patch("/tasks/" + result.draggableId, {
            column: result.destination.droppableId,
        });

        setTaskMap(
            () =>
                reorderTaskMap({
                    taskMap: taskMap,
                    source,
                    destination,
                }).taskMap
        );
    };

    return (
        <div>
            <AddTaskModal
                isVisible={isAddTaskModalVisible}
                onAddTask={(id: string, content: string) => {
                    const newTodos = [
                        ...Object.values(taskMap)[0],
                        { id, content },
                    ];
                    const firstKey = Object.keys(taskMap)[0];
                    taskMap[firstKey] = newTodos;

                    setTaskMap(taskMap);
                }}
                onClose={() => setIsAddTaskModalVisible(false)}
            />
            <div className="mb-4 flex justify-end">
                <Button
                    className="flex items-center gap-2"
                    onClick={() => setIsAddTaskModalVisible(true)}
                >
                    Add task
                    <Plus size={20} />
                </Button>
            </div>
            <DragDropContext onDragEnd={onDragEnd}>
                <div className="flex items-start gap-4 w-full">
                    {Object.entries(taskMap).map((column, index) => (
                        <div className="w-1/4" key={index}>
                            <TaskList
                                title={columnNames[index]}
                                listId={column[0]}
                                listType="card"
                                tasks={column[1]}
                                isDropDisabled={false}
                            />
                        </div>
                    ))}
                </div>
            </DragDropContext>
        </div>
    );
};

import { Container } from "@/components/shared/Container";
import { Footer } from "@/components/shared/Footer";
import { Header } from "@/components/shared/Header";
import Link from "next/link";
import React, { ComponentPropsWithoutRef, FC } from "react";

interface ClientLayoutPropsType extends ComponentPropsWithoutRef<"div"> {}

const ClientLayout: FC<ClientLayoutPropsType> = ({ children }) => {
    return (
        <Container className="flex flex-col min-h-screen">
            <Header />
            <main className="flex-1 flex gap-8">
                <aside className="flex flex-col gap-4">
                    <Link className="font-bold hover:underline" href="/">
                        Dashboard
                    </Link>
                    <Link
                        className="font-bold hover:underline"
                        href="/students"
                    >
                        Students
                    </Link>
                    <Link className="font-bold hover:underline" href="/tasks">
                        Tasks
                    </Link>
                </aside>
                <div className="flex-1">{children}</div>
            </main>
            <Footer />
        </Container>
    );
};

export default ClientLayout;

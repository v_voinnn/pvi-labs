"use client";

import React, { useState } from "react";

interface UserData {
    id: number;
    firstName: string;
    lastName: string;
    gender: string;
    group: string;
    status: "online" | "offline";
    selected?: boolean; // Optional property for row selection
}

export const Table: React.FC = () => {
    const [tableData, setTableData] = useState<UserData[]>([
        {
            id: 1,
            firstName: "John",
            lastName: "Doe",
            gender: "Male",
            group: "A",
            status: "online",
        },
        {
            id: 2,
            firstName: "Jane",
            lastName: "Smith",
            gender: "Female",
            group: "B",
            status: "offline",
        },
        // Add more objects for additional rows
    ]);

    const [selectAll, setSelectAll] = useState(false);

    const handleSelectRow = (id: number) => {
        setTableData(
            tableData.map((row) =>
                row.id === id ? { ...row, selected: !row.selected } : row
            )
        );
    };

    const handleSelectAll = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSelectAll(e.target.checked);
        setTableData(
            tableData.map((row) => ({ ...row, selected: e.target.checked }))
        );
    };

    return (
        <div className="container mx-auto">
            <table className="table-auto w-full text-left">
                <thead>
                    <tr className="bg-gray-500 text-gray-100">
                        <th className="px-4 py-2">
                            <input
                                type="checkbox"
                                checked={selectAll}
                                onChange={handleSelectAll}
                            />
                        </th>
                        <th className="px-4 py-2">Name</th>
                        <th className="px-4 py-2">Gender</th>
                        <th className="px-4 py-2">Group</th>
                        <th className="px-4 py-2">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {tableData.map((row) => (
                        <tr
                            key={row.id}
                            className={
                                row.selected ? "bg-gray-200" : "bg-white"
                            }
                        >
                            <td className="px-4 py-2">
                                <input
                                    type="checkbox"
                                    checked={row.selected}
                                    onChange={() => handleSelectRow(row.id)}
                                />
                            </td>
                            <td className="px-4 py-2">
                                {row.firstName} {row.lastName}
                            </td>
                            <td className="px-4 py-2">{row.gender}</td>
                            <td className="px-4 py-2">{row.group}</td>
                            <td className="px-4 py-2 text-xs font-bold text-green-500">
                                {row.status === "online" ? "Online" : "Offline"}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

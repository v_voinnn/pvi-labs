import { Table } from "./_components/Table";

const StudentsPage = () => {
    return (
        <div>
            <Table />
        </div>
    );
};

export default StudentsPage;

import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Header } from "@/components/shared/Header";
import { Footer } from "@/components/shared/Footer";
import { cn } from "@/lib/utils";
import { Container } from "@/components/shared/Container";
import { AuthProvider } from "@/components/providers/AuthContext";
import { Toaster } from "@/components/ui/toaster";
import {className} from "postcss-selector-parser";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
    title: "PVI",
    description: "PVI labs",
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <AuthProvider>
                <body className={cn("bg-slate-300",inter.className)}>
                    {children}
                    <Toaster />
                </body>
            </AuthProvider>
        </html>
    );
}

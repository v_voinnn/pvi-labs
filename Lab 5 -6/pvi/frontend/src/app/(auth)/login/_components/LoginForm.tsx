"use client";

import { AuthContext } from "@/components/providers/AuthContext";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { useToast } from "@/components/ui/use-toast";
import { api } from "@/lib/api";
import React, { useContext } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Link from "next/link";

interface IFormInput {
    username: string;
    password: string;
}

export const LoginForm: React.FC = () => {
    const { login } = useContext(AuthContext);
    const { toast } = useToast();

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<IFormInput>();

    const onSubmit: SubmitHandler<IFormInput> = async (data) => {
        console.log(data);

        if (!(data.username && data.password)) {
            alert("Please fill all fields");
            toast({
                title: "Login failed",
                description: "all fields are required",
                variant: "destructive",
            });
            return;
        }

        try {
            const loginData = await api.post<{
                isSuccess: boolean;
                _id: string;
            }>("/auth/login", {
                username: data.username,
                password: data.password,
            });

            if (loginData.data.isSuccess) {
                toast({
                    title: "Success!",
                    description: "You are logged in",
                });

                login(loginData.data._id);
                window.location.href = "/";
            }
        } catch (e) {
            toast({
                title: "Error",
                description: "Sth gone wrong. Try later",
            });
        }
    };

    return (
        <div className="flex items-center justify-center min-h-screen bg-gray-100">
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="bg-white p-6 rounded shadow-md w-full max-w-sm"
            >
                <h2 className="text-2xl mb-6 text-center">Login</h2>

                <div className="mb-4">
                    <label className="block text-gray-700">Username</label>
                    <Input
                        type="text"
                        {...register("username", { required: true })}
                        className={`mt-1 block w-full ${
                            errors.username
                                ? "border-red-500"
                                : "border-gray-300"
                        }`}
                    />
                    {errors.username && (
                        <p className="text-red-500 text-sm mt-1">
                            Username is required
                        </p>
                    )}
                </div>

                <div className="mb-4">
                    <label className="block text-gray-700">Password</label>
                    <Input
                        type="password"
                        {...register("password", {
                            required: true,
                            minLength: 8,
                        })}
                        className={`mt-1 block w-full ${
                            errors.password
                                ? "border-red-500"
                                : "border-gray-300"
                        }`}
                    />
                    {errors.password && errors.password.type === "required" && (
                        <p className="text-red-500 text-sm mt-1">
                            Password is required
                        </p>
                    )}
                    {errors.password &&
                        errors.password.type === "minLength" && (
                            <p className="text-red-500 text-sm mt-1">
                                Password must be at least 8 characters
                            </p>
                        )}
                </div>

                <Button
                    type="submit"
                    className="w-full bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
                >
                    Login
                </Button>


            </form>
        </div>
    );
};

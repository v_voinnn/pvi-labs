const { db } = require("../db/db");

class StudentsService {
  async getAllStudents() {
    try {
      const { data, error } = await db.from("students").select("*");

      if (error) {
        throw new Error("Can't get all students");
      }

      return data;
    } catch (error) {
      throw new Error("Can't get all students");
    }
  }

  async getUserById(studentId) {
    if (!studentId) throw new Error("Id is required");

    try {
      const { data, error } = await db.from("students").select("*").eq("id", studentId).single();

      return data;
    } catch (error) {
      throw new Error("Can't get user with this id");
    }
  }

  async editUserById(userId, newUser) {
    if (!userId) throw new Error("Id is required");
    if (!newUser) throw new Error("User data is required");

    try {
      const { error } = await db.from("students").update(newUser).eq("id", userId).select();

      if (error) {
        throw new Error("Can't edit user with this id");
      }

      return {
        id: userId,
        ...newUser,
      };
    } catch (error) {
      throw new Error("Can't edit user with this id");
    }
  }

  async addStudent(userData) {
    if (!userData) throw new Error("User data is required");

    try {
      const { data, error } = await db.from("students").insert([userData]).select();

      if (error) {
        throw new Error("Can't create this user");
      }

      return data?.[0];
    } catch (error) {
      console.log("Error", error);
      throw new Error("Can't create this user");
    }
  }

  async deleteStudent(studentId) {
    if (!studentId) throw new Error("User data is required");

    try {
      const { error } = await db.from("students").delete().eq("id", studentId);

      if (error) {
        throw new Error("Can't delete this user");
      }

      return studentId;
    } catch (error) {
      throw new Error("Can't delete this user");
    }
  }
}

module.exports = {
  studentsService: new StudentsService(),
};

const containsNumbersOrNonAlphaSymbols = (str) => {
  const regex = /[\d\u0020-\u0040\u005B-\u0060\u007B-\u00FF]/;

  return regex.test(str);
};

const validateBirthday = (birthday) => {
  const birthdayDate = new Date(birthday);

  const currentDate = new Date();

  const age = currentDate.getFullYear() - birthdayDate.getFullYear();

  if (age < 16) {
    return "You are to young";
  } else if (age > 80) {
    return "You are to old";
  } else {
    return false;
  }
};

const validateUserData = (user) => {
  let error;

  if (!user.group) error = "Group is required";
  else if (!user.firstName) error = "First name is required";
  else if (containsNumbersOrNonAlphaSymbols(user.firstName))
    error = "First name should contain only letters";
  else if (!user.lastName) error = "Last name is required";
  else if (containsNumbersOrNonAlphaSymbols(user.lastName))
    error = "Last name should contain only letters";
  else if (user.isMale === undefined) error = "Gender is required";
  else if (!user.birthday) error = "Birthday is required";
  else if (validateBirthday(user.birthday)) {
    error = validateBirthday(user.birthday);
  }

  return error;
};

module.exports = {
  validateUserData,
};
